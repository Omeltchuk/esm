@extends('layouts.main')
@section('content')
    <!-- Blog Post -->
    <div class="card mb-4">
        <div class="card-body">
            <h2 class="card-title">{{ $post->title }}</h2>
            <p class="card-text">{{ $post->body }}</p>
            @can('update', $post)
                <a href="{{ route('posts.edit', $post->slug) }}" class="btn btn-sm btn-outline-primary">Edit post</a>
            @endcan
        </div>
        <div class="card-footer text-muted">
            Author: {{ $post->author }}
        </div>
        <div class="card-footer text-muted">
            Tags:
            @foreach($post->tags as $tag)
                <a href="{{ route('posts.index', ['tag' => $tag->name]) }}">{{ $tag->name }}</a>
            @endforeach
        </div>
    </div>
@endsection