@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <h2>Create new post</h2>
                    <div class="panel-body">
                        {!! Form::open(['route' => ['posts.store'], 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'post-store']) !!}
                        <div class="form-group">
                            <label for="title" class="col-md-12 control-label">Title</label>

                            <div class="col-md-12">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus
                                       v-validate="'required|alpha_spaces|max:255'" @keyup="hideErrors"
                                       :class="{'is-invalid': errors.has('title') }">

                                <span class="text-danger">
                                    @if ($errors->has('title'))
                                        <strong v-show="displayErrors">{{ $errors->first('title') }}</strong>
                                    @endif
                                    <strong v-cloak v-show="errors.has('title')">@{{ errors.first('title') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="body" class="col-md-12 control-label">Body</label>

                            <div class="col-md-12">
                                <textarea id="body" form="post-store" type="text" name="body" required rows="15" class="form-control"
                                          v-validate="'required|max:65535'" @keyup="hideErrors"
                                          :class="{'is-invalid': errors.has('body') }">
                                    {{ old('body') }}
                                </textarea>

                                <span class="text-danger">
                                    @if ($errors->has('body'))
                                        <strong v-show="displayErrors">{{ $errors->first('body') }}</strong>
                                    @endif
                                    <strong v-cloak v-show="errors.has('body')">@{{ errors.first('body') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tags" class="col-md-12 control-label">Tags</label>

                            <div class="col-md-12">
                                {!! Form::select('tags[]', $tags, null, [ 'class' => 'form-control select2', 'multiple' => 'multiple']) !!}

                                @if ($errors->has('tags'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('tags') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-5">
                                <button type="submit" class="btn btn-outline-primary"
                                        @click.prevent="validateBeforeSubmit($event)">
                                    Create new post
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endpush
@push('footer_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://unpkg.com/vee-validate@2.0.0-rc.7"></script>
    <script src="{{ asset('js/vue/validation.js') }}"></script>
    <script src="{{ asset('js/posts/selectTags.js') }}"></script>
@endpush
