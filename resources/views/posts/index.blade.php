@extends('layouts.main')
@section('content')
    @foreach($posts as $post)
        <!-- Blog Post -->
        <div class="card mb-4">
            <div class="card-body">
                <h3 class="card-title">{{ $post->title }}</h3>
                <p class="card-text">{{ str_limit($post->body, 100) }}</p>
                <a href="{{ route('posts.show', $post->slug) }}" class="btn btn-sm btn-outline-primary">Read More &rarr;</a>
                @can('update', $post)
                    <a href="{{ route('posts.edit', $post->slug) }}" class="btn btn-sm btn-outline-info">Edit</a>
                @endcan
                @can('delete', $post)
                    {!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'DELETE', 'class' => 'd-inline']) !!}
                    <button class='btn btn-sm btn-outline-danger'>Delete</button>
                    {!! Form::close() !!}
                @endcan
            </div>
            <div class="card-footer text-muted">
                Author: {{ $post->author }}
            </div>
            <div class="card-footer text-muted">
                Tags:
                @foreach($post->tags as $tag)
                    <a href="{{ route('posts.index', ['tag' => $tag->name]) }}">{{ $tag->name }}</a>
                @endforeach
            </div>
        </div>
    @endforeach
    {{ $posts->links() }}
@endsection