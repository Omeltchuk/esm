@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <h2>Create new user</h2>
                    <div class="panel-body">
                        {!! Form::open(['route' => ['users.store'], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label for="name" class="col-md-12 control-label">Name</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus
                                       v-validate="'required|alpha_spaces|max:255'" @keyup="hideErrors"
                                       :class="{'is-invalid': errors.has('name') }">

                                <span class="text-danger">
                                    @if ($errors->has('name'))
                                        <strong v-show="displayErrors">{{ $errors->first('name') }}</strong>
                                    @endif
                                    <strong v-cloak v-show="errors.has('name')">@{{ errors.first('name') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                                       v-validate="'required|email|max:255'" @keyup="hideErrors"
                                       :class="{'is-invalid': errors.has('email') }">

                                <span class="text-danger">
                                    @if ($errors->has('email'))
                                        <strong v-show="displayErrors">{{ $errors->first('email') }}</strong>
                                    @endif
                                    <strong v-cloak v-show="errors.has('email')">@{{ errors.first('email') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-12 control-label">Password</label>

                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control" name="password"
                                       v-validate="'required|confirmed|max:255|min:6'" @keyup="hideErrors"
                                       :class="{'is-invalid': errors.has('password') }">

                                <span class="text-danger">
                                    @if ($errors->has('password'))
                                        <strong v-show="displayErrors">{{ $errors->first('password') }}</strong>
                                    @endif
                                    <strong v-cloak v-show="errors.has('password')">@{{ errors.first('password') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-12 control-label">Confirm Password</label>

                            <div class="col-md-8">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="is_admin" class="col-md-12 control-label">Has admin permissions</label>

                            <div class="col-md-8">
                                <input id="is_admin" type="checkbox" class="form-control" name="is_admin" value="1">

                                <span class="text-danger">
                                    @if ($errors->has('is_admin'))
                                        <strong v-show="displayErrors">{{ $errors->first('is_admin') }}</strong>
                                    @endif
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-5">
                                <button type="submit" class="btn btn-primary" @click.prevent="validateBeforeSubmit($event)">
                                    Create new user
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer_scripts')
    <script src="https://unpkg.com/vee-validate@2.0.0-rc.7"></script>
    <script src="{{ asset('js/vue/validation.js') }}"></script>
@endpush
