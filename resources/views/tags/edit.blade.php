@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <h2>Tag edit</h2>
                    <div class="panel-body">
                        {!! Form::open(['route' => ['tags.update', $tag->id], 'method' => 'PATCH', 'class' => 'form-horizontal']) !!}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-12 control-label">Name</label>

                            <div class="col-md-12">
                                <input id="name" type="text" name="name" value="{{ $tag->name }}" required autofocus class="form-control"
                                       v-validate="'required|alpha_dash|max:255'" @keyup="hideErrors"
                                       :class="{'is-invalid': errors.has('name') }">

                                <span class="text-danger">
                                    @if ($errors->has('name'))
                                        <strong v-show="displayErrors">{{ $errors->first('name') }}</strong>
                                    @endif
                                    <strong v-cloak v-show="errors.has('name')">@{{ errors.first('name') }}</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-5">
                                <button type="submit" class="btn btn-outline-primary" @click.prevent="validateBeforeSubmit($event)">
                                    Update tag
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('footer_scripts')
    <script src="https://unpkg.com/vee-validate@2.0.0-rc.7"></script>
    <script src="{{ asset('js/vue/validation.js') }}"></script>
@endpush
