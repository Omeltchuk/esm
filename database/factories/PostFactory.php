<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Post::class, function (Faker $faker) {
    $title = $faker->sentence;
    $slug = \Illuminate\Support\Str::slug($title);

    return [
        'user_id' => rand(1, 10),
        'title' => $title,
        'body' => $faker->paragraph(200),
        'slug' => $slug,
        'views' => rand(0, 10)
    ];
});
