<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        factory(App\User::class, 100)->create();
        factory(App\Models\Post::class, 100)->create()->each(function ($u) {
            $u->tags()->save(factory(\App\Models\Tag::class)->make());
        });;
    }
}
