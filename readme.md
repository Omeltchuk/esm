
## Test Blog

    Requirments:
    - PHP 7.1.3 or higher.
    
### Get started:

- Clone.
- Setup your web server.
- Create .env from .env.example.
- Setup db connection in .env.
- Execute command: 'composer install' from project directory.
- Execute command: 'php artisan key:generate' from project directory. 
- Execute command: 'php artisan migrate' from project directory.
- Execute command: 'php artisan db:seed' from project directory.

### Test data

- Admin credentials: login - admin@admin.com, password - secret.