Vue.use(VeeValidate);
var app = new Vue({
    el: '#app',
    data: {
        displayErrors: true
    },
    methods: {
        validateBeforeSubmit: function (event) {
            this.$validator.validateAll().then(function (result) {
                if (result)
                    event.target.form.submit();
            });
        },
        hideErrors: function () {
            this.displayErrors = false;
        }
    }
});