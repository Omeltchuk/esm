<?php

Auth::routes();
Route::get('/', 'PostController@index')->name('home');
Route::resource('posts', 'PostController')->except(['show', 'edit']);
Route::group(['prefix' => 'posts', 'as' => 'posts.'], function () {
    Route::get('{slug}', 'PostController@show')->name('show');
    Route::get('{slug}/edit', 'PostController@edit')->name('edit');
});
Route::resource('users', 'UserController')->except(['show'])->middleware('admin');
Route::resource('tags', 'TagController')->except(['show'])->middleware('admin');;
