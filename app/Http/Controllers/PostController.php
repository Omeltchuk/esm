<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Models\Tag;
use App\Repositories\PostsRepository;
use App\Repositories\Criteria\RelationAttribute;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $postsRepository;

    public function __construct(PostsRepository $postsRepository)
    {
        $this->postsRepository = $postsRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($tag = $request->input('tag'))
            $this->postsRepository->pushCriteria(new RelationAttribute('tags', 'name', $tag));

        $posts = $this->postsRepository->paginate(5);
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Post::class);
        $tags = Tag::all()->pluck('name', 'id');
        return view('posts.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $this->authorize('create', Post::class);
        $data = $request->only(['title', 'body']);
        $data['user_id'] = \Auth::user()->id;
        $data['slug'] = Post::slugify($data['title']);
        $this->postsRepository->createWithRelations($data, ['tags' => $request->tags]);

        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = $this->postsRepository->findBy('slug', $slug);
        $post->incrementViews();
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $post = $this->postsRepository->findBy('slug', $slug);
        $this->authorize('update', $post);
        $tags = Tag::all()->pluck('name', 'id');

        return view('posts.edit', compact('post', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PostRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $this->authorize('update', Post::findOrFail($id));
        $data = $request->only(['title', 'body']);
        $data['slug'] = Post::slugify($data['title']);
        $this->postsRepository->updateWithRelations($data, $id, ['tags' => $request->tags]);

        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', Post::findOrFail($id));
        $post = $this->postsRepository->find($id);
        $post->tags()->detach();
        $post->delete();
        return back();
    }
}
