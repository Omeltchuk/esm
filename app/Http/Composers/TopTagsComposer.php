<?php

namespace App\Http\Composers;

use Illuminate\Contracts\View\View;

/**
 * Order category composer
 *
 * @package App\Http\Composers
 */
class TopTagsComposer
{
    /**
     * @var null
     */
    private $data = null;

    /**
     * Prepares the data
     *
     */
    public function getData()
    {
        if ($this->data !== null) {
            return $this->data;
        }

        // get top 5 tags by total views of related posts
        $this->data = \DB::table('tags')
            ->join('post_tag', 'post_tag.tag_id', '=', 'tags.id')
            ->join('posts', 'post_tag.post_id', 'posts.id')
            ->select(\DB::raw('sum(posts.views) as total, tags.name'))
            ->groupBy('tags.id')
            ->orderBy('total', 'desc')
            ->limit(5)
            ->get();

        return $this->data;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('topTags', $this->getData());
    }

}

