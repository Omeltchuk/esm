<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'is_admin' => 'boolean',
            'password' => 'required|string|min:6|confirmed',
        ];
        switch ($this->method()) {
            case 'POST':
                {
                    $rules['email'] = 'required|string|email|max:255|unique:users,email';
                }
            case 'PUT':
            case 'PATCH':
                {
                    $rules['email'] = 'required|string|email|max:255|unique:users,email,' . $this->route('user');
                }
            default:
                break;
        }

        return $rules;
    }
}
