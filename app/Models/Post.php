<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'body', 'user_id'
    ];

    /*
     * Generates an unique slug for post
     */
    public static function slugify($title)
    {
        $slug = str_slug($title, '-');
        if ($post = self::where('slug', $slug)->first())
            $slug .= '-' . ++$post->id;

        return $slug;
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getAuthorAttribute()
    {
        return optional($this->user)->name ?? 'Anonymous';
    }

    public function incrementViews()
    {
        $this->views++;
        return $this->save();
    }
}
