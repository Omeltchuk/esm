<?php

namespace App\Traits\Repositories;


trait Relations
{

    /**
     * @param array $data
     * @param array $relations
     * @return mixed
     */
    public function createWithRelations(array $data, array $relations)
    {
        $model = parent::create($data);

        foreach ($relations as $relationName => $relationIds) {
            $model->{$relationName}()->attach($relationIds);
        }

        return $model;
    }

    /**
     * @param array $data
     * @param $id
     * @param array $relations
     * @param string $attribute
     * @return mixed
     */
    public function updateWithRelations(array $data, $id, $relations, $attribute = "id")
    {
        parent::update($data, $id, $attribute);

        if (!($model = $this->model->find($id))) {
            return false;
        }

        foreach ($relations as $relationName => $relationIds) {
            $model->{$relationName}()->sync($relationIds);
        }

        return $model;
    }
}