<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Post;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($user, $ability)
    {
        if ($user->is_admin)
            return true;
    }

    public function create($user)
    {
        return \Auth::check();
    }

    public function update($user, Post $post)
    {
        return $user->id === optional($post->user)->id;
    }

    public function delete($user, Post $post)
    {
        return $user->id === optional($post->user)->id;
    }
}
