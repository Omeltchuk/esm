<?php

namespace App\Repositories;

use App\Models\Tag;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class TagsRepository extends Repository {

    public function model() {
        return Tag::class;
    }
}