<?php

namespace App\Repositories;

use App\Models\Post;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use App\Traits\Repositories\Relations;

class PostsRepository extends Repository {

    use Relations;

    public function model() {
        return Post::class;
    }
}