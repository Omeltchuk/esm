<?php

namespace App\Repositories\Criteria;

use Bosnadev\Repositories\Criteria\Criteria;
use Bosnadev\Repositories\Contracts\RepositoryInterface as Repository;


class RelationAttribute extends Criteria {

    protected $attribute;
    protected $value;
    protected $relation;
    protected $condition;

    public function __construct($relation, $attribute, $value, $condition = '=')
    {
        $this->attribute = $attribute;
        $this->value = $value;
        $this->relation = $relation;
        $this->condition = $condition;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model->with($this->relation)->whereHas($this->relation, function ($query){
            $query->where($this->attribute, $this->condition, $this->value);
        });

        return $query;
    }
}