<?php

namespace App\Repositories;

use App\User;
use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

class UsersRepository extends Repository {

    public function model() {
        return User::class;
    }
}